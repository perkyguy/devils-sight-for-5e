const DevilsSightFor5e = {
  sightLayerUpdate: SightLayer.prototype.update,

  patch: {
    removeLitTokenRemnants: (tokenId) => {
      canvas.sight.sources.lights.delete(`Token.${tokenId}`);
      canvas.sight.update();
      canvas.lighting.update();
    },
  },

  devilsSight: () => {
    const controlledToken = canvas.tokens.controlled[0];

    if (controlledToken && controlledToken.data.brightRadius) {
      const darknessLights = canvas.sight.light.black.children;
      darknessLights.forEach(l => {
        l.light.blendMode = PIXI.BLEND_MODES.ERASE;
      });
    }
  },
};

SightLayer.prototype.update = function update() {
  DevilsSightFor5e.sightLayerUpdate.bind(this)();
  DevilsSightFor5e.devilsSight();
};

Hooks.on('setup', DevilsSightFor5e.setup);
Hooks.on('ready', () => {
  Hooks.on('deleteToken', (scene, token) => DevilsSightFor5e.patch.removeLitTokenRemnants(token._id));
});
